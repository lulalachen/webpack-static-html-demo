const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const OUTPUT_DIR = path.resolve(__dirname, 'build/')

module.exports = {
  entry: {
    index: './src/js/index.js'
  },
  devServer: {
    contentBase: OUTPUT_DIR,
    hot: true
  },
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: OUTPUT_DIR
  },
  resolve: {
    alias: {
      css: path.resolve(__dirname, 'src/css'),
      js: path.resolve(__dirname, 'src/js'),
      utils: path.resolve(__dirname, 'src/utils'),
      images: path.resolve(__dirname, 'src/images')
    },
  },
  module: {
    rules: [
      {
        test: /\.(html)$/,
        use: [
          'html-loader'
        ]
      },
      {
        test: /\.js$/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
}
