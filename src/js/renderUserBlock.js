// Use library
import R from 'ramda'
import 'css/UserBlock.css'

async function getUsers () {
  const usersResponse = await fetch('http://jsonplaceholder.typicode.com/users')
  const jsonBody = usersResponse.json()
  return jsonBody
}

const renderUserBlock = () =>
  getUsers()
    .then(R.take(7)) // equals to: .then((users) => R.take(5)(users))
    .then(users =>
      users.map(user =>
        `
          <div class='userBlock'>
            <div class='username'>${user.name}</div>
            <div class='email'>${user.email}</div>
          </div>
        `
      ).join('')
    )
    .then(userBlocks =>
      `
        <div class='userBlockWrap'>
          ${userBlocks}
        </div>
      `
    )


export default renderUserBlock
