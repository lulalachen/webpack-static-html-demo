import R from 'ramda'
import renderHTMLIntoDiv from 'utils/renderHTMLIntoDiv'
import { appendTo, replaceTo } from 'utils/documentTool'
import renderUserBlock from './renderUserBlock'

const router = (hash) => {
  const path = hash.replace('#', '')
  console.log('Routing to ', path)
  switch (path) {
    case '/about': {
      const aboutHTML = `<div class='container'> About Page </div>`
      R.pipe(
        renderHTMLIntoDiv,
        replaceTo('root'),
      )(aboutHTML)
      break
    }
    case '/':
    default: {
      renderUserBlock()
        .then(
          R.pipe(
            renderHTMLIntoDiv,
            replaceTo('root'),
          )
        )
      break
    }
  }
}
export default router
