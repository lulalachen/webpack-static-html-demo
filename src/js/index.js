import R from 'ramda'
import sayHello, { sayNo } from './sayHello'
// 相當於：
// const sayHello = require('./sayHello').default
// const sayNo = require('./sayHello').sayNo
import renderUserBlock from './renderUserBlock'
import renderHTMLIntoDiv from 'utils/renderHTMLIntoDiv'

import { appendTo } from 'utils/documentTool'
import router from './router'

import catImage from 'images/cat_die.jpg'

import 'css/global.css'
import 'css/NavBar.css'

console.log(sayHello('lulala'))
console.log(sayNo('poka'))


// Render NavBar
const Nav = `
  <div class='wrap'>
    <img src=${catImage} alt="icon" />
    HTML Test
    <a href='#/'>Home</a>
    <a href='#/about'>About</a>
  </div>
`
R.pipe(
  renderHTMLIntoDiv,
  appendTo('nav'),
)(Nav)



// Initial Route
router(window.location.hash)

// Add Route listener
const linkButtons = document.querySelectorAll('a')
linkButtons.forEach(button =>
  button.addEventListener('click', (e) => {
    e.preventDefault()
    window.history.pushState({}, '', e.target.href)
    router(window.location.hash)
  })
)
