export const appendTo = (id) => (node) => document.getElementById(id).appendChild(node)
export const replaceTo = (id) => (node) => {
  const div = document.getElementById(id)
  div.innerHTML = ''
  div.appendChild(node)
}
